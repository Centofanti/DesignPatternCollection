# Struttura del repository
Il repository è suddiviso in packages, ognuno di essi è chiamato con il nome del relativo design pattern.
Ogni cartella costituisce un package a se stante ed è fornito di un file principale con un main ed un codice di esempio runnabile.
I metodi delle classi specializzate stampano a video il loro nome e/o cosa stanno facendo.
Se necessario, anche il client stampa indicazioni su cosa sta per fare

## Design pattern presenti:
* Strategy
* Decorator
* Adapter
* Facade
* Observer
* Factory
* AbstractFactory